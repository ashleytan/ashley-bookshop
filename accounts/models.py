from django.db import models

from shop.models import Book

# Create your models here.
class Order(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    customer = models.CharField(max_length=100)
    charge = models.DecimalField(decimal_places=2, max_digits=6)
