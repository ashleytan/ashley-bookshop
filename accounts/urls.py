from django.conf.urls import url

from . import views

app_name = 'accounts'

urlpatterns = [
    url(r'(?P<pk>\d+)/$', views.buy_book, name="buy"),
]