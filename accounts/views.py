from django.shortcuts import get_object_or_404, render

from shop.models import Book

# Create your views here.
def buy_book(request, pk):
    book = get_object_or_404(Book, pk=pk)
    return render(request, "accounts/invoice.html", {"book": book})