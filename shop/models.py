from django.db import models

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=100)
    category = models.CharField(max_length=30)
    price = models.DecimalField(decimal_places=2, max_digits=6)
    blurb = models.TextField()
    author = models.CharField(max_length=30)
    created_at = models.DateField()