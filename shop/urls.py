from django.conf.urls import url

from . import views

app_name = 'shop'

urlpatterns = [
    url(r'(?P<pk>\d+)/$', views.view_book, name="view"),
    url(r'^$', views.display_all_books, name="all"),
]