from django.shortcuts import get_object_or_404, render

from .models import Book

# Create your views here.
def display_all_books(request):
    books = Book.objects.all()
    return render(request, "shop/display_all_books.html" ,{"books": books})

def view_book(request, pk):
    book = get_object_or_404(Book, pk=pk)
    return render(request, "shop/view_book.html", {"book": book})


